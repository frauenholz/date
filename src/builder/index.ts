/** Package information retrieved from `package.json` using webpack. */
declare const PACKAGE_NAME: string;
declare const PACKAGE_VERSION: string;

/** Dependencies */
import {
    DateTime as DateTimeHelper,
    Forms,
    NodeBlock,
    Slots,
    affects,
    conditions,
    definition,
    each,
    editor,
    insertVariable,
    isBoolean,
    isNumber,
    pgettext,
    slots,
    tripetto,
} from "tripetto";
import { IDateTime } from "../runner";
import { TMode } from "../runner/mode";
import { DateTimeCondition } from "./condition";

/** Assets */
import ICON from "../../assets/icon.svg";

@tripetto({
    type: "node",
    identifier: PACKAGE_NAME,
    version: PACKAGE_VERSION,
    icon: ICON,
    get label() {
        return pgettext("block:date", "Date");
    },
})
export class DateTime extends NodeBlock implements IDateTime {
    dateSlot!: Slots.Date;

    @definition("boolean", "optional")
    @affects("#label")
    @affects("#slots")
    time?: boolean;

    @definition("boolean", "optional")
    @affects("#label")
    @affects("#slots")
    range?: boolean;

    @definition("number", "optional")
    @affects("#slots")
    minimum?: number | true;

    @definition("number", "optional")
    @affects("#slots")
    maximum?: number | true;

    @definition("boolean", "optional")
    @affects("#slots")
    required?: boolean;

    @definition("string", "optional")
    @affects("#slots")
    alias?: string;

    @definition("boolean", "optional")
    @affects("#slots")
    exportable?: boolean;

    @definition("string", "optional")
    placeholder?: string;

    get label() {
        if (this.range) {
            return pgettext("block:date", "Date range");
        }

        if (this.time) {
            return pgettext("block:date", "Date with time");
        }

        return this.type.label;
    }

    static getToday(s: "begin" | "end"): number {
        return DateTimeHelper.UTCToday + (s === "end" ? 86400000 - 1 : 0);
    }

    @slots
    defineSlot(): void {
        this.dateSlot = this.slots.static({
            type: Slots.Date,
            reference: "date",
            label: this.range
                ? pgettext("block:date", "Date from")
                : this.time
                ? pgettext("block:date", "Date with time")
                : DateTime.label,
            required: this.required,
            alias: this.alias,
            exportable: this.exportable,
            exchange: [
                "required",
                "alias",
                "exportable",
                "precision",
                "minimum",
                "maximum",
            ],
        });

        this.dateSlot.precision = this.time ? "minutes" : "days";
        this.dateSlot.minimum = this.minimum;
        this.dateSlot.maximum = this.maximum;

        if (this.range) {
            const toSlot = this.slots.static({
                type: Slots.Date,
                reference: "to",
                label: pgettext("block:date", "Date to"),
                required: this.required,
                alias: this.alias,
                exportable: this.exportable,
                exchange: [
                    "required",
                    "alias",
                    "exportable",
                    "precision",
                    "minimum",
                    "maximum",
                ],
                pipe: "to",
            });

            toSlot.precision = this.dateSlot.precision;
            toSlot.minimum = this.dateSlot.minimum;
            toSlot.maximum = this.dateSlot.maximum;
        } else {
            this.slots.delete("to", "static");
        }
    }

    @editor
    defineEditor(): void {
        const features = (time?: boolean) =>
            Forms.DateTimeFeatures.Date |
            (time
                ? Forms.DateTimeFeatures.TimeHoursAndMinutesOnly
                : Forms.DateTimeFeatures.Weekday);
        const placeholder = new Forms.Text(
            "singleline",
            Forms.Text.bind(this, "placeholder", undefined, "")
        )
            .placeholder(
                pgettext(
                    "block:date",
                    "Type placeholder for the range to date field..."
                )
            )
            .action("@", insertVariable(this))
            .visible(this.range || false);

        this.editor.name();
        this.editor.description();
        this.editor.placeholder(placeholder);
        this.editor.explanation();

        this.editor.groups.settings();
        this.editor.option({
            name: pgettext("block:date", "Time"),
            form: {
                title: pgettext("block:date", "Time"),
                controls: [
                    new Forms.Checkbox(
                        pgettext("block:date", "Allow a time to be set"),
                        Forms.Checkbox.bind(this, "time", undefined, true)
                    ).on((time) => {
                        minimum.features(
                            features(time.isFeatureEnabled && time.isChecked)
                        );
                        maximum.features(
                            features(time.isFeatureEnabled && time.isChecked)
                        );
                    }),
                    new Forms.Static(
                        pgettext(
                            "block:date",
                            "When enabled this will allow the user to set a specific time along with the date."
                        )
                    ),
                ],
            },
            activated: isBoolean(this.time),
        });

        this.editor.option({
            name: pgettext("block:date", "Range"),
            form: {
                title: pgettext("block:date", "Range"),
                controls: [
                    new Forms.Checkbox(
                        pgettext("block:date", "Enable date range"),
                        Forms.Checkbox.bind(this, "range", undefined, true)
                    ).on(() => {
                        placeholder.visible(this.range || false);
                    }),
                    new Forms.Static(
                        pgettext(
                            "block:date",
                            "When enabled this will display two date input fields. One for the *from* date and another one for the *to* date."
                        )
                    ).markdown(),
                ],
            },
            activated: isBoolean(this.range),
        });

        const minimum = new Forms.DateTime(
            isNumber(this.minimum) ? this.minimum : undefined
        )
            .zone("UTC")
            .features(features(this.time))
            .disabled(this.minimum === true)
            .years(
                new Date().getFullYear() - 150,
                new Date().getFullYear() + 50
            )
            .width("full")
            .label(pgettext("block:date", "Minimum"))
            .placeholder(pgettext("block:date", "Not set"))
            .on(() => {
                if (minimum.isFeatureEnabled) {
                    if (this.minimum !== true) {
                        this.minimum = minimum.value;
                    }
                } else {
                    this.minimum = undefined;
                }
            });
        const maximum = new Forms.DateTime(
            isNumber(this.maximum) ? this.maximum : undefined
        )
            .zone("UTC")
            .features(features(this.time))
            .disabled(this.maximum === true)
            .years(
                new Date().getFullYear() - 150,
                new Date().getFullYear() + 50
            )
            .width("full")
            .label(pgettext("block:date", "Maximum"))
            .placeholder(pgettext("block:date", "Not set"))
            .on(() => {
                if (maximum.isFeatureEnabled) {
                    if (this.maximum !== true) {
                        this.maximum = maximum.value;
                    }
                } else {
                    this.maximum = undefined;
                }
            });

        this.editor.option({
            name: pgettext("block:date", "Limits"),
            form: {
                title: pgettext("block:date", "Limits"),
                controls: [
                    minimum,
                    maximum,
                    new Forms.Static(pgettext("block:date", "Options")),
                    new Forms.Checkbox(
                        pgettext("block:date", "Date must be in the future"),
                        this.minimum === true
                    ).on((future) => {
                        if (future.isFeatureEnabled && future.isChecked) {
                            this.minimum = true;
                        } else if (this.minimum === true) {
                            this.minimum = undefined;

                            minimum.refresh();
                        }

                        minimum.disabled(this.minimum === true);
                    }),
                    new Forms.Checkbox(
                        pgettext("block:date", "Date must be in the past"),
                        this.maximum === true
                    ).on((past) => {
                        if (past.isFeatureEnabled && past.isChecked) {
                            this.maximum = true;
                        } else if (this.maximum === true) {
                            this.maximum = undefined;

                            maximum.refresh();
                        }

                        maximum.disabled(this.maximum === true);
                    }),
                ],
            },
            activated:
                isNumber(this.minimum) ||
                this.minimum === true ||
                isNumber(this.maximum) ||
                this.maximum === true,
        });

        this.editor.groups.options();
        this.editor.required(this);
        this.editor.visibility();
        this.editor.alias(this);
        this.editor.exportable(this);
    }

    @conditions
    defineCondition(): void {
        const date = this.slots.select("date");
        const to = this.slots.select("to");
        const templates: { mode: TMode; label: string }[] = [
            {
                mode: "equal",
                label: pgettext("block:date", "Is equal to"),
            },
            {
                mode: "not-equal",
                label: pgettext("block:date", "Is not equal to"),
            },
            {
                mode: "before",
                label: pgettext("block:date", "Is before"),
            },
            {
                mode: "after",
                label: pgettext("block:date", "Is after"),
            },
            {
                mode: "between",
                label: pgettext("block:date", "Is between"),
            },
            {
                mode: "not-between",
                label: pgettext("block:date", "Is not between"),
            },
            {
                mode: "defined",
                label: pgettext("block:date", "Is not empty"),
            },
            {
                mode: "undefined",
                label: pgettext("block:date", "Is empty"),
            },
        ];

        if (date) {
            const group = to
                ? this.conditions.group(
                      date.label || pgettext("block:date", "Date from")
                  )
                : this.conditions;

            each(templates, (condition) => {
                group.template({
                    condition: DateTimeCondition,
                    label: condition.label,
                    autoOpen:
                        condition.mode !== "defined" &&
                        condition.mode !== "undefined",
                    props: {
                        slot: date,
                        mode: condition.mode,
                        value: this.time
                            ? DateTimeHelper.UTC
                            : DateTimeHelper.UTCToday,
                        to:
                            condition.mode === "between" ||
                            condition.mode === "not-between"
                                ? this.time
                                    ? DateTimeHelper.UTC
                                    : DateTimeHelper.UTCToday
                                : undefined,
                    },
                });
            });
        }

        if (to) {
            const group = this.conditions.group(
                to.label || pgettext("block:date", "Date to")
            );

            each(templates, (condition: { mode: TMode; label: string }) => {
                group.template({
                    condition: DateTimeCondition,
                    label: condition.label,
                    autoOpen:
                        condition.mode !== "defined" &&
                        condition.mode !== "undefined",
                    props: {
                        slot: to,
                        mode: condition.mode,
                        value: DateTimeHelper.UTC,
                        to:
                            condition.mode === "between" ||
                            condition.mode === "not-between"
                                ? DateTimeHelper.UTC
                                : undefined,
                    },
                });
            });
        }
    }
}
