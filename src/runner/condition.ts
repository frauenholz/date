/** Package information retrieved from `package.json` using webpack. */
declare const PACKAGE_NAME: string;

/** Dependencies */
import {
    ConditionBlock,
    DateTime,
    Num,
    Slots,
    condition,
    isNumberFinite,
    isString,
    tripetto,
} from "tripetto-runner-foundation";
import { TMode } from "./mode";

export interface IDateTimeCondition {
    readonly mode: TMode;
    readonly value?: number | string;
    readonly to?: number | string;
}

@tripetto({
    type: "condition",
    identifier: PACKAGE_NAME,
})
export class DateTimeCondition extends ConditionBlock<IDateTimeCondition> {
    private getValue(slot: Slots.Date, value: number | string | undefined) {
        if (isString(value) && slot instanceof Slots.Date) {
            const variable = this.variableFor(value);

            return variable && variable.hasValue
                ? slot.toValue(variable.value)
                : undefined;
        }

        return slot.toValue(isNumberFinite(value) ? value : DateTime.UTC);
    }

    @condition
    verify(): boolean {
        const dateSlot = this.valueOf<number, Slots.Date>();

        if (dateSlot) {
            const value = this.getValue(dateSlot.slot, this.props.value);
            const input = dateSlot.hasValue
                ? dateSlot.slot.toValue(dateSlot.value)
                : undefined;

            switch (this.props.mode) {
                case "equal":
                    return input === value;
                case "not-equal":
                    return input !== value;
                case "before":
                    return (
                        isNumberFinite(value) &&
                        isNumberFinite(input) &&
                        input < value
                    );
                case "after":
                    return (
                        isNumberFinite(value) &&
                        isNumberFinite(input) &&
                        input > value
                    );
                case "between":
                case "not-between":
                    const to = this.getValue(dateSlot.slot, this.props.to);

                    return (
                        isNumberFinite(value) &&
                        isNumberFinite(to) &&
                        (isNumberFinite(input) &&
                            input >= Num.min(value, to) &&
                            input <= Num.max(value, to)) ===
                            (this.props.mode === "between")
                    );
                case "defined":
                    return dateSlot.hasValue;
                case "undefined":
                    return !dateSlot.hasValue;
            }
        }

        return false;
    }
}
